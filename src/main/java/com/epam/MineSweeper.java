package com.epam;

import java.util.Scanner;

public class MineSweeper {
    private String[][] field = new String[12][12];
    private String[][] display = new String[12][12];
    private Boolean isDone = false;
    private Boolean isWin = false;

    private String unknown = " ? ";
    private String mine = " * ";
    private String empty = "   ";

    public MineSweeper(int bombAmount) {
        for (int x = 0; x < field.length; x++) {
            for (int y = 0; y < field[0].length; y++) {
                if ((x == 0 || x == field.length - 1) || (y == 0 || y == field[0].length - 1)) {
                    field[x][y] = empty;
                    display[x][y] = empty;
                } else {
                    field[x][y] = unknown;
                    display[x][y] = unknown;
                }
            }
        }
    }

    public static void printGame(String[][] str) {
        for (int x = 1; x < str.length - 1; x++) {
            for (int y = 0; y < str[0].length; y++) {
                if (y > 0 && y < str[0].length)
                    System.out.print("|");
                else
                    System.out.println();

                System.out.print(str[x][y]);
            }
        }
    }

    public void update() {
        printGame(display);
        System.out.println();
    }

    public int generateMines(int n) {
        for (int m = 0; m < n; m++) {
            while (true) {
                int x, y = 0;
                x = (int) (10 * Math.random());
                y = (int) (10 * Math.random());

                if (x >= 1 && x <= 10) {
                    if (y >= 1 && y <= 10) {
                        if (!field[x][y].equals(mine)) {
                            field[x][y] = mine;
                            break;
                        }
                    }
                }
            }
        }
        return n;
    }

    public void clear(int x, int y) {
        for (int i = (x - 1); i <= (x + 1); i++) {
            for (int j = (y - 1); j <= (y + 1); j++) {
                if (field[i][j].equals(unknown) == true) {
                    display[i][j] = empty;
                    field[i][j] = empty;
                }
            }
        }
    }

    public String getTile(int x, int y) {
        return field[x][y];
    }

    public void detect() {
        for (int x = 1; x < display.length - 2; x++) {
            for (int y = 1; y < display.length - 2; y++) {
                if (field[x][y].equals(empty) == true) {
                    int nums = 0;
                    for (int i = (x - 1); i <= (x + 1); i++) {
                        for (int j = (y - 1); j <= (y + 1); j++) {
                            if (field[i][j].equals(mine) == true)
                                nums++;
                        }
                    }
                    display[x][y] = " " + nums + " ";
                }
            }
        }
    }

    public void turn(int x, int y) {
        if (field[x][y].equals(unknown) == true) {
            isDone = false;
            display[x][y] = empty;
            field[x][y] = empty;
        } else if (field[x][y].equals(mine) == true) {
            isDone = true;
            isWin = false;
            System.out.println("Програв!");
        } else if (display[x][y].equals(empty) == true && field[x][y].equals(empty)) {
            isDone = false;
            System.out.println("Вже було!");
        }
    }

    public void isVictory() {
        int tile = 0;
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[0].length; j++) {
                if (field[i][j].equals(unknown) == true)
                    tile++;
            }
        }
        if (tile != 0)
            isWin = false;
        else {
            isWin = true;
            isDone = true;
        }
    }

    public Boolean getDone() {
        return isDone;
    }

    public Boolean getWin() {
        return isWin;
    }

    public void onEnd() {
        printGame(field);
    }

    public static void main(String[] args) {
        MineSweeper bombGenerator = null;
        int bombAmount = bombGenerator != null ? bombGenerator.generateMines(16) : 16;
        MineSweeper mineSweeper = new MineSweeper(bombAmount);
        mineSweeper.update();
        Scanner scan = new Scanner(System.in);

        int x, y;
        System.out.println("Координата по іксу?");
        x = scan.nextInt();
        System.out.println("Координата по ігрику?");
        y = scan.nextInt();

        if (mineSweeper.getTile(x, y).equals(" * ") == true) {
            mineSweeper.generateMines(1);
            mineSweeper.field[x][y] = " ? ";
        }

        mineSweeper.clear(x, y);
        mineSweeper.detect();
        mineSweeper.update();

        while (true) {
            if (mineSweeper.getDone() == true && mineSweeper.getWin() == true) {
                System.out.println("Winner winner chicken dinner!");
                mineSweeper.onEnd();
                break;
            } else if (mineSweeper.getDone() == true) {
                mineSweeper.onEnd();
                break;
            } else if (mineSweeper.getDone() == false) {
                x = -1;
                y = -1;
                System.out.println("Координата по іксу?");
                y = scan.nextInt();
                System.out.println("Координата по ігрику?");
                x = scan.nextInt();
                mineSweeper.turn(x, y);
                mineSweeper.isVictory();
                mineSweeper.detect();
                mineSweeper.update();
            }
        }
    }
}
