package com.epam;

public class LargestPlataeu {
    public static void main(String[] args) {
        int[] value = new int[args.length];
        for(int i = 0 ; i < args.length; i++){
            value[i] = Integer.parseInt(args[i]);
        }
        //print index
        for(int i = 0 ; i <args.length;i++){
            System.out.print(i+ " ");
        }
        //print values at index
        System.out.println();
        for(int i = 0 ; i <args.length;i++){
            System.out.print(value[i]+ " ");
        }
        System.out.println();

        int inp_length = value.length;
        int len_big_pt = 3;
        int big_p1=1;
        int big_p2=2;
        //if platuea exists it can start anywhere between 0 and length-3. Can't exist after that
        for(int p1 = 0; p1 < inp_length-3;p1++){
            //Plataue cant start at the first 3 locations
            for(int p2 = p1+3;p2< inp_length;p2++){
                boolean condition1=false;
                boolean condition2=true;
                //value at the end of the plateau can be less or equal to value at the begining .
                if(value[p2]<=value[p1]){
                    condition1 = true;
                    //all middle values equal and higher than 1st value
                    for(int p = p1+2;p <p2;p++){

                        condition2 = true;
                        if((value[p]!=value[p-1]||value[p]<=value[p1])){
                            condition2 = false;

                            break;
                        }
                    }
                }
                if(condition1&&condition2){
                    System.out.println(p1+" "+p2+ "  val[p1] "+ value[p1]+ "  val[p2] "+ value[p2]);
                    System.out.println("plateau");
                    int pt_len = p2 - p1;
                    if(pt_len >= len_big_pt){
                        len_big_pt = pt_len;
                        big_p1 = p1;
                        big_p2 = p2;
                    }
                }
            }
        }
        if(len_big_pt>0){
            System.out.println("Biggest plateau exists from "+big_p1+" to "+big_p2);
        }
        else{
            System.out.println("No plateau");
        }
    }
}