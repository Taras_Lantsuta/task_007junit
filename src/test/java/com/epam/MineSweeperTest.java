package com.epam;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Field;

@ExtendWith(MockitoExtension.class)
public class MineSweeperTest {
    private static Class clazz;
    private static MineSweeper mineSweeper;
    private static int bombAmount = 16;
    @Mock
    private MineSweeper mineController;

    @BeforeAll
    public static void init(){
        mineSweeper = new MineSweeper(bombAmount);
        clazz = mineSweeper.getClass();
    }

    @Test
    public void constant() throws NoSuchFieldException, IllegalAccessException {
        final Field hide = clazz.getDeclaredField("hide");
        hide.setAccessible(true);
        Assertions.assertEquals("#", hide.get(mineSweeper));
    }

}
